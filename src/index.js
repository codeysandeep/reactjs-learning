/* // ====================================================

// How to Import React and ReactDOM in JavaScript

var React = require("react");
var ReactDOM = require("react-dom");

// ==================================================== */

/* // =======================================================

// How to Import React and ReactDOM in ES6

import React from "react";
import ReactDOM from "react-dom";

// ======================================================= */

/* // =========================================================

// How to render an element in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <center>Hello programmers, Welcome to React World!</center>,
  document.getElementById("root")
);

// ======================================================== */

/* // =========================================================

// JavaScript XML (JSX) in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <center>Hello programmers, Welcome to React World!</center>,
  document.getElementById("root")
);

// ========================================================= */

/* // =========================================================

// How to render multiple elements in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <center>
    <p>Twinkle, twinkle, little star</p>
    <p>How I wonder what you are</p>
    <p>Up above the world so high</p>
    <p>Like a diamond in the sky</p>
  </center>,
  document.getElementById("root")
);

// ========================================================= */

/* // =========================================================

// How to render an array of elements in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  [
    <center>Twinkle, twinkle, little star</center>,
    <center>How I wonder what you are</center>,
    <center>Up above the world so high</center>,
    <center>Like a diamond in the sky</center>,
  ],
  document.getElementById("root")
);

// ========================================================= */

/* // ========================================================

// Fragments in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <React.Fragment>
    <center>Twinkle, twinkle, little star</center>
    <center>How I wonder what you are</center>
    <center>Up above the world so high</center>
    <center>Like a diamond in the sky</center>
  </React.Fragment>,
  document.getElementById("root")
);

// ======================================================== */

/* // ========================================================

// Syntactic Sugar for Fragments in React

import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <>
    <center>Twinkle, twinkle, little star</center>
    <center>How I wonder what you are</center>
    <center>Up above the world so high</center>
    <center>Like a diamond in the sky</center>
  </>,
  document.getElementById("root")
);

// ======================================================= */

/* // =======================================================

// JSX Expressions in React

import React from "react";
import ReactDOM from "react-dom";

const name = "Codey Sandeep";
const age = 21;
const course = "B.Tech (CSE)";

ReactDOM.render(
  <center>
    <p>Name : {name}</p>
    <p>Age : {age}</p>
    <p>Course : {course}</p>
  </center>,
  document.getElementById("root")
);

// ======================================================= */

/* // =========================================================

// Template Literals in React

import React from "react";
import ReactDOM from "react-dom";

const name = "Codey Sandeep";
const age = 21;
const course = "B.Tech";
const branch = "CSE";

ReactDOM.render(
  <center>
    <p>{`Name : ${name}`}</p>
    <p>{`Age : ${age}`}</p>
    <p>{`Course : ${course} (${branch})`}</p>
  </center>,
  document.getElementById("root")
);

// ======================================================== */

/* // ======================================================

// JSX Attributes in React

import React from "react";
import ReactDOM from "react-dom";

let source = "https://picsum.photos/200/300";
let link = "https://www.google.com";

ReactDOM.render(
  <center>
    <p contentEditable="true">Hello programmers, Welcome to React World!</p>
    <a href={link}>
      <img src={source} alt="Random Beauty from Lorem Picsum" />
    </a>
  </center>,
  document.getElementById("root")
);

// ====================================================== */

/* // ======================================================

// How to Import CSS in React

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const source = "https://picsum.photos/200/300";
const text = "Random Beauty from Lorem Picsum";

ReactDOM.render(
  <center>
    <p className="title">Hello programmers, Welcome to React World!</p>
    <img src={source} alt={text} className="image" />
  </center>,
  document.getElementById("root")
);

// ===================================================== */

/* // =====================================================

// Inline CSS Styling in React

import React from "react";
import ReactDOM from "react-dom";

const source = "https://picsum.photos/200/300";
const text = "Random Beauty from Lorem Picsum";

const title = {
  fontSize: "17px",
  color: "#000000",
  textShadow: "0px 2px 4px #ccc",
};

const image = {
  border: "5px double #000000",
  borderRadius: "20px",
};

ReactDOM.render(
  <center>
    <p style={title}>Hello programmers, Welcome to React World!</p>
    <img src={source} alt={text} style={image} />
  </center>,
  document.getElementById("root")
);

// ======================================================= */

/* // =======================================================

// Components in React

import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import App from "./App";

ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById("root")
);

// ===================================================== */

/* // =====================================================

// Import/Export in React

import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import { Heading, Image } from "./TutorialA1";

ReactDOM.render(
  <center>
    <Heading />
    <Image />
  </center>,
  document.getElementById("root")
);

// ==================================================== */

/* // ====================================================

// Props in React

import React from "react";
import ReactDOM from "react-dom";
import App from "./TutorialA2";

import "./index.css";

ReactDOM.render(
  <>
    <App
      library="React"
      source="https://picsum.photos/200/300"
      text="Random Beauty from Lorem Picsum"
    />
  </>,
  document.getElementById("root")
);

// =================================================== */

/* // ===================================================

// Props using Array in React

import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import { Fruits, Card } from "./TutorialA3";

ReactDOM.render(
  <div className="container-fluid">
    <Card title={Fruits[0].name} price={Fruits[0].price} />
    <Card title={Fruits[1].name} price={Fruits[1].price} />
    <Card title={Fruits[2].name} price={Fruits[2].price} />
    <Card title={Fruits[3].name} price={Fruits[3].price} />
    <Card title={Fruits[4].name} price={Fruits[4].price} />
  </div>,
  document.getElementById("root")
);

// =================================================== */

/* // ====================================================

// Props using Array Map function in React

import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import { Fruits, Card } from "./TutorialA3";

ReactDOM.render(
  <div className="container-fluid">
    {Fruits.map(function Cards(fruit) {
      return <Card title={fruit.name} price={fruit.price} />;
    })}
  </div>,
  document.getElementById("root")
);

// ==================================================== */

/* // =====================================================

// Props using Fat Arrow function in React

import React from "react";
import ReactDOM from "react-dom";

import { Fruits, Card } from "./TutorialA3";
import "./style.css";

ReactDOM.render(
  <div className="container-fluid">
    {Fruits.map((fruit) => {
      return <Card key={fruit.id} title={fruit.name} price={fruit.price} />;
    })}
  </div>,
  document.getElementById("root")
);

// ====================================================== */

/* // ======================================================

// Conditional rendering in React

import React from "react";
import ReactDOM from "react-dom";

import "./style.css";
import Text from "./TutorialA4";

ReactDOM.render(
  <div className="container-fluid">
    <div className="card">
      <Text />
    </div>
  </div>,
  document.getElementById("root")
);

// ====================================================== */

/* // ========================================================

// Conditional rendering using Ternary Operator

import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import Text from "./TutorialA5";

ReactDOM.render(
  <div className="container-fluid">
    <div className="card">
      <Text />
    </div>
  </div>,
  document.getElementById("root")
);

// ====================================================== */

/* // ======================================================

// Hooks in React

import React from "react";
import ReactDOM from "react-dom";

import "./style.css";
import App from "./TutorialA6";

ReactDOM.render(
  <div className="container">
    <App />
  </div>,
  document.getElementById("root")
);

// ===================================================== */

/* // ======================================================

// Events in React

import React from "react";
import ReactDOM from "react-dom";

import "./style.css";
import App from "./TutorialA7";

ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById("root")
);

// ====================================================== */

/* // =======================================================

// Forms in React

import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import App from "./TutorialA8";

ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById("root")
);

// ===================================================== */

/* // =====================================================

// Handling Multiple Inputs in Form in React

import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import App from "./TutorialA9";

ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById("root")
);

// ==================================================== */
