import React from "react";

let marks = 78;

const Text = () => {
  return (
    <div className="card-text">
      {marks >= 70 ? "Congratulations, You Scored!" : "Sorry, Try Again!"}
      <i class="fas fa-hand-point-up"></i>
    </div>
  );
};

export default Text;
