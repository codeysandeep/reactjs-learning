import React from "react";

function Heading() {
  return <p className="title">Hello programmers, Welcome to React World!</p>;
}

const source = "https://picsum.photos/200/300";
const text = "Random Beauty from Lorem Picsum";

function Image() {
  return <img src={source} alt={text} className="image" />;
}

export { Heading, Image };
