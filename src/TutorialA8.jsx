import React, { useState } from "react";

const App = () => {
  const [name, setName] = useState("");
  const [username, setUsername] = useState();

  const inputEvent = (event) => {
    setName(event.target.value);
  };

  const UpdateName = (event) => {
    event.preventDefault();
    setUsername(name);
  };

  return (
    <>
      <div className="container">
        <form onSubmit={UpdateName}>
          <p className="title">Good morning, {username}</p>
          <input
            type="text"
            placeholder="What is your name?"
            onChange={inputEvent}
            value={name}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    </>
  );
};

export default App;
