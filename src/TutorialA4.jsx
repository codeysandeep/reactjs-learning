import React from "react";

const age = 35;

function Text() {
  if (age > 0 && age < 18) {
    return (
      <>
        <div className="card-text">You are not eligible to drive!</div>
        <i class="fas fa-hand-point-up"></i>
      </>
    );
  } else if (age >= 18 && age <= 100) {
    return (
      <>
        <div className="card-text">You are eligible to drive!</div>
        <i class="fas fa-hand-point-up"></i>
      </>
    );
  } else {
    return (
      <>
        <div className="card-text">Invalid Age, You can't Alive!</div>
        <i class="fas fa-hand-point-up"></i>
      </>
    );
  }
}

export default Text;
