import React from "react";

function App(props) {
  return (
    <center>
      <p className="title">
        Hello programmers, Welcome to {props.library} World!
      </p>
      <img className="image" src={props.source} alt={props.txt} />
    </center>
  );
}

export default App;
