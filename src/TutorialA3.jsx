import React from "react";

const Fruits = [
  {
    id: 1,
    name: "Apple",
    price: "$20",
  },
  {
    id: 2,
    name: "Banana",
    price: "$12",
  },
  {
    id: 3,
    name: "Grapes",
    price: "$45",
  },
  {
    id: 4,
    name: "Mango",
    price: "$23",
  },
  {
    id: 5,
    name: "Papaya",
    price: "$54",
  },
];

function Card(props) {
  return (
    <div className="card">
      <h4 className="card-title">{props.title}</h4>
      <div className="card-price">{props.price}</div>
    </div>
  );
}

export { Fruits, Card };
