import React, { useState } from "react";

const App = () => {
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");

  const [fullname, setFullname] = useState();
  const [fullsurname, setFullsurname] = useState();

  const nameInputEvent = (event) => {
    setName(event.target.value);
  };

  const surnameInputEvent = (event) => {
    setSurname(event.target.value);
  };

  const UpdateFullname = (event) => {
    event.preventDefault();
    setFullname(name);
    setFullsurname(surname);
  };

  return (
    <>
      <div className="container">
        <form onSubmit={UpdateFullname}>
          <p className="title">
            Good morning, {fullname} {fullsurname}
          </p>
          <input
            type="text"
            placeholder="What is your name?"
            onChange={nameInputEvent}
            value={name}
          />
          <input
            type="text"
            placeholder="What is your surname?"
            onChange={surnameInputEvent}
            value={surname}
            style={{ marginTop: "-3px" }}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    </>
  );
};

export default App;
