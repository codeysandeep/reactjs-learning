import React, { useState } from "react";

const App = () => {
  const [count, setCount] = useState(0);

  const Counter = () => {
    setCount(count + 1);
  };

  return (
    <>
      <h1 className="number">{count}</h1>
      <button onClick={Counter}>Click Me</button>
    </>
  );
};

export default App;
