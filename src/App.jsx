import React from "react";

const source = "https://picsum.photos/200/300";
const text = "Random Beauty from Lorem Picsum";

function App() {
  return (
    <center>
      <p className="title">Hello programmers, Welcome to React World!</p>
      <img src={source} alt={text} className="image" />
    </center>
  );
}

export default App;
