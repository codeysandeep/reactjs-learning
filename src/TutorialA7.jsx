import React, { useState } from "react";

const App = () => {
  const orangered = "orangered";

  const [color, changeColor] = useState(orangered);

  const ChangeToLime = () => {
    const lime = "lime";
    changeColor(lime);
  };

  const ChangeToBlue = () => {
    const blue = "blue";
    changeColor(blue);
  };

  return (
    <>
      <div className="container">
        <i class="fab fa-centos" style={{ color: color }}></i>
        <button
          onClick={ChangeToLime}
          onDoubleClick={ChangeToBlue}
          style={{ backgroundColor: color }}
        >
          Click Me
        </button>
      </div>
    </>
  );
};

export default App;
